「那個,你是,從一開始就一直是這個模樣的嗎?」

聽到春雪的疑問,衣衫凌亂橫躺著的貌美的大天使,將那那白金色的睫毛稍稍抬起了一丁點兒.【SR:這事後的既視感是怎麼回事!!?】

「我沒聽懂這個疑問的意思啊,僕人」

「啊,那個,也就是說,你畢竟活了八千年了,在這期間裡沒有經歷過種種進化之類的事嗎」

「進化……?那是指你們這些活在底端層級的存在的世代性變化的詞,用在像我這般永恒性個體的身上不合適吧」

冷漠無情地斷言了的梅塔特隆,將春雪那粉紅色的假想體拎了起來,放在了轉身朝上的自己的身上.

雖說兩人同是在《初期加速空間》中的假想體,但感覺到了那像是將他包裹在內的溫暖以及柔軟后,春雪的心跳就不由得急速上升.

可是如果這被她知道了一定會痛斥為「無禮的傢伙!」,所以盡全力讓自己的意識從那觸感上轉移並邊回答道.

「那個,雖然確實如此……但從以前開始,在現實世界(底端層級)被製作出的遊戲……啊不對是其他的世界中,個體也是會進化的啊」

在春雪腦海中浮現的當然是,主角的伙伴怪獸們以《XX進化!》而變強,變大的情形.說不定梅塔特隆一開始也是柔弱的小女生,在經過了數次進化後才成為了現在的完全體(或者說是究極體)也說不定,春雪僅是偶然如此想到.

不過這最強的神獸(legend)級公敵大人,露出一副深入沉思的表情後,以黃金色的眼瞳盯向了放在自己胸口的春雪.

「哼.在中間層級中度過的漫長的時光中,若是《變化》的確經歷過相應的.與眾多小戰士相戰,知曉了世界的廣闊……但是《進化》指的是,向前進一步發展的意思吧?我的真身現如今依然還在我城中的最深處被囚禁著,無法朝著任何方向前進……」

「…………這樣啊.說的也是啊……」

春雪因自己那不經大腦的疑問而羞愧,垂下了頭.

不過大天使這一次卻是將垂著頭的春雪的假想體用雙手抱住後,浮現出罕見的沉穩笑容說道.

「……不過呢,現在我像這樣,豈止是城裡,還脫離出了中間層級,與身為其他世界的人的你相依相靠.這種心靈相通的感覺,或許會讓我得到初次的進化呢」

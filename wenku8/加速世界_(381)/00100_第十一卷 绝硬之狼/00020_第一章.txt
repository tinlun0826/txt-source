二〇四七年六月二十四日，星期一，下午六點三十分。

蓋在杉並區高圓寺北部的住商混合高層大廈B棟二十三樓，有田家的客廳裡，春雪最重視的一群好伙伴——也就是「黑暗星雲」軍團的團員——全都到齊了。

六人座的餐桌旁，最靠裡的上座是軍團長黑雪會主；靠廚房的兩張椅子坐著參謀黛拓武與開心果倉嶋千百合；靠陽台的一邊則坐著軍團的良心兼吉祥物四堃宮謠與（儘管他自己也不願意）麻煩製造機春雪；而在黑雪公主的正對面，則坐著副軍團長倉崎楓子。

正好就在一周前的星期一，春雪認識了第六名團員謠，此後這個座次也就自然而然地成了慣例。然而，今天黑雪公主與楓子身旁，卻分別多準備了一張椅子，餐桌上的盤子也是八個，而不是本來的六個。

已經跳到保溫模式的電子鍋飄散出米飯的甜香，電磁爐上的大鍋也散發出強烈得堪稱暴力的香料芬芳。春雪是不用說了，連其他五人也一臉飽受折磨的表情，談話有一句沒一句。

「…………我……頂不住了……」

春雪昏頭轉向地這麼一說，身旁的謠隨即無力地動了動十根手指。

【UI〉要忍耐，鴉鴉。這也是鍛鏈精神力的訓練sy】

儘管謠表情堅毅，但這難得一見的錯字，顯示出她也快要忍不住了；坐在正對面的千百合翻白眼瞪著白色的盤子，拓武則一直擦著眼鏡鏡片，一旁楓子嘴角的微笑也愈來愈可怕。在這樣的情勢下，黑雪公主卻閉著眼睛一動也不動，表現出軍團長的風範——

「…………太慢了！」

但這只是假象，她在餐桌上輕輕一拍，大聲喊道：

「已經遲到三分三十秒了！要是在加速世界，已經晚了五十八小時啊！」

「說得精確一點，是五十八小時二十分鐘。」

楓子笑嘻嘻地補上一句。春雪似乎能看見她們兩人背上冒出火焰般的鬥氣，忍不住照平時的習性幫忙緩頰：

「學、學姊就別生氣了，師父也是。怎怎怎麼說呢？畢竟大家都說咖哩放愈久愈好吃。」

「喔？那就把你的份放個夠吧？放個三天試試看。」

「難得有這個機會，我們就試著超越極限，放個一周吧。」

「這、這樣會超越另外一種極限啦！」

就在春雪慌慌張張亂揮雙手的瞬間，所有人的聽覺都接收到了期待已久的音效。叮咚兩響門鈴聲尚未結束，春雪的右手已經快如閃電地劃過，按下視野中顯示的來客視窗開鎖按鈕。

「歡、歡迎！我會去電梯前面等，你們直接上二十三樓來！」

接著春雪就以幾乎要從椅子上跌下來似的勢頭衝向玄關，背後的五人也站了起來。黑雪公主右手一揮，迅速指揮眾人：

「千百合去盛飯！拓武跟楓子把沙拉從冰箱拿出來！謠謠去把咖哩重新加熱！麥茶交給我來準備！」

春雪將人領到客廳後，訪客說出的第一句話是——

「哇，好香喔！我肚子都餓扁了啦！」

黑暗星雲的眾人投來充滿殺氣的視線，這位來賓卻絲毫不為所動。她只是抬頭看著春雪，以天真的笑容說下去：

「大哥哥，我要坐哪邊才好？」

春雪趕緊推著那裡了火紅T恤的雙肩，讓她在黑雪公主身旁的椅子坐下。這個座位安排不免有點劍拔弩張，但考慮到餐桌靠裡的位子是上座，也就不得不讓她一起坐在那兒。

因為，這名蹦蹦跳跳坐到椅子上的紅髮雙馬尾少女，在立場上與黑雪公主完全同等。她正是「日珥」軍團的首領紅之王——「不動要塞」Scarlet　Rain——上月由仁子。

仁子肯乖乖坐在黑雪公主身旁，讓春雪先鬆了口氣，不過緊接著第二位來賓便無聲無息地出現在客廳門口。這人之所以會比仁子晚了幾十秒出現，多半是因為要先在玄關脫下厚重的騎士靴。她的雙手戴著黑色的長款皮手套，跟短袖水手意外地十分搭調。她將垂在肩上的辮子撥到身後，以略低的女低音說：

「SRY（sorry）。環七發生車禍回避塞車。」

這個說話能省則省的人物，當然就是紅色軍團的副軍團長Blood　Leopard。簡稱Pard小姐。

春雪正要走回去帶她入座，離她最近的楓子卻搶先站起說道：

「這可辛苦你了，Leopard。陷在那邊的車陣可是很難脫身的呢。」

楓子以平靜的語氣說話，同時走到Pard小姐身前。

所謂車禍回避塞車，指的是即將發生車禍時，車輛控制AI會緊急煞車，同時對周圍的車輛也發出停車訊號，因而造成塞車的情形。當道路車流量太高，停車訊號就會形成連鎖反應不斷散播，讓極大範圍內的車輛都停下來，再不然就是進入強制慢行模式。

此刻的有田家客廳，這兩名女性面對面所發出的隱形火花也在瞬間散播開來，讓春雪的動作緊急煞車。咽下口水的他這才意識到一件事。

綽號「血腥小貓」綽號的Pard小姐與綽號「鐵腕」的Sky　Raker倉崎楓子，直剄第一代黑暗星雲垮台前，都相知相惜地把彼此當成最好的對手——之前他是這麼聽說的。而且，Pard小姐雖然資歷相當深，等級卻留在6級，這點似乎與Raker的半退隱狀態有很深的關係。

「……Hi。Raker.」

Pard小姐脫下手套，簡短地打了聲招呼。從她的動作上，完全看不出這兩人到底是曾在現實中相見，還是今天才初次見面。

至少，在三周前「赫密斯之索縱貫賽」的最終盤，她們兩人就曾見面並講過幾句話，但那是有許多隊伍參加的大賽型對戰。仔細想想，從Raker回歸以後，這兩人始終沒有直接進行過任何一次對戰。

……該、該該該不會要在這裡當場開打吧？

春雪不由得手心捏了把冷汗，但坐在右後方的仁子以解除了「天使模式」的聲調，很乾脆地打消了這劍拔弩張的氣氛。

「你們別對瞪個沒完沒了，趕快來吃吧！我等不下去了啦！」

「……我們等得才久了呢，紅之王。」

她身旁的黑雪公主隨即這麼回答。楓子也趁這時退開一步，要Pard小姐就坐。互為勁敵的兩人並肩坐下後，春雪也趕緊回到自己座位上，這時黑雪公主才再度開口：

「那麼，不管怎麼說，先吃飯吧。有事吃完再說……開動。」

「「「開動！」」」

其餘七人也大聲應和，同時拿起湯匙，迎向擺在各人眼前的大盤咖哩飯。

為什麼黑暗星雲全團六人會親手做咖哩飯，邀請日珥的兩大巨頭同桌用餐呢？

理由要回溯到昨天——六月二十三日，星期日所召開的「七王會議」。

春雪大口咀嚼著自己親手挑選、削皮的馬鈴薯，同時將記憶倒轉二十小時，回溯起那只要走錯一步，說不定就會被純色諸王宣判死刑的法庭……

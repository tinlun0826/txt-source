然後到了第二天。

我正坐在特等席上喝著提供的早晨咖啡。咖啡目前貌似只有三越商會能生產。真厲害呢。

「美味」

順帶一提我是多放牛奶和砂糖派的。

特等席、雖然一開始很討厭但習慣了以後還真是方便呢。只要拜託女僕的話大致上所有東西都能夠免費送來，還能暫時品味到名流的感覺。

感受了一段時間的會場氛圍之後愛麗絲公主登場了。

「早上好」

「早上好—」

「咖啡嗎。最近很流行呢。雖然聞起來很香，但由於不怎麼擅長苦味我……」

「那只要多加些牛奶和砂糖弄成咖啡牛奶就行了哦」

「咖啡牛奶嗎……？」

說著便叫來女僕進行嘗試的愛麗絲公主。真有行動力呢。

「啊、真美味……」

「對吧。這可是無論什麼樣的咖啡，都能夠全部變成同樣味道的魔法呢」

就以這種感覺順帶拜託了吐司和雞蛋享受了時髦的早餐。

如果有SNS的話，再上傳張和王族在特等座享受早餐一臉得意的照片就完美了呢。

早餐結束後名流們斷斷續續地入場了。

然後名流們便開始了社交寒磣。男爵家的我理所當然地無法插入對話中被孤立了起來。沒事啦，反正也沒想插進去。所以還請不要把話題轉到我這邊啦愛麗絲大人。

在這微妙地有點不自在的環境中，迎來了正賽第二天的開始的時刻。

正當名流們也做到了座位上空氣變得稍微平靜的時候，特等房間的門打開了。

轉過身去，在那裡站著一位身著褪色斗篷的女性。

雖然一如既往因為頭蓬無法看見臉部，但她的確是貝雅托麗克絲。

她注意到我之後稍稍揮了揮手，我點了點頭露出了微笑。又見面了呢、這樣的感覺。

但是特等席的名流們眼光卻很嚴厲。

仿彿能夠聽見‘這個穿著髒兮兮頭蓬的傢伙是誰啊，快點把她給趕出去’這樣的聲音。這就是無言的壓力嗎。

「這位客人，雖然很失禮……」

就在女僕向她搭話的時候。

「沒關係。那一位是我招待過來的客人。這邊請」

愛麗絲想貝雅托麗克絲搭話了。

貝雅托麗克絲坐在了愛麗絲旁邊。愛麗絲夾在中間而在我的正對側。順帶一提那裡貌似是阿蕾克西雅的座位。

「愛麗絲大人、那一位是……」

「這位是『武神』貝雅托麗克絲大人」

在愛麗絲回答了名流的疑問之後，在他們之間引起了一陣騷然。

「她就是、那個……」

「被稱為『武神』的……」

「傳說中的劍聖……」

哦哦、感覺有點帥啊。我也想試著被別人「他就是那個傳說中的Shadow……」這樣說說看！

「貝雅托麗克絲大人像這樣登上台前真是久違了呢」

「嗯。正在找人」

對於名流的疑問她點了點頭如此答道。

「在找長得和我很像的侄女」

為了不犯同樣的錯誤她把臉上的頭蓬揭了下來。

「哦哦、這可真是美麗……」

「這張臉、有見過嗎？貌似最近這個國家有人見過長得很像的精靈」

「嚯、這個國家嗎……貝雅托麗克絲大人這般美麗的精靈只要見過一次應該不會忘記的吧」

「沒有、印象嗎？」

「非常遺憾……」

名流們一同搖了搖頭。

「是嗎……」

她遺憾地披上了斗篷。

「對不起。在這裡的各位都是面子廣泛的人，本以為在這裡打聽的話就能夠知道些什麼……」

愛麗絲向貝雅托麗克絲致歉道。

「沒事。畢竟精靈有的是時間」

「話說回來，貝雅托麗克絲大人有看『武神祭』的比賽嗎？」

「沒怎麼看」

「是嗎。只要是在知道的範圍就可以了，能說說您有什麼注目的選手嗎？」

「注目的選手……嗚嗯——」

她環顧了會場稍微考慮了一下。

「希德」

然後指向了我。

「那個、貝雅托麗克絲大人……？」

「對希德比較注目。一定會變強的」

「不，不會啦」

我立馬否定了。

周圍的視線好痛。

「這個少年會變強……？」

「這位是我的後輩，但資質實在是……」

「姑且是克蕾婭的弟弟，但該說是沒有才能呢還是……」

「貝雅托麗克絲大人這麼說了的話，那大概沒錯吧」

愛麗絲大人的一句話姑且結束了這微妙的氣氛。

但是、名流們看貝雅托麗克絲的眼神帶上了懷疑。

這傢伙是真貨嗎？

仿彿在用視線進行著這樣的對話一般。

在他們的眼中她恐怕只是個看上去髒兮兮的流浪者吧。

但在我看來、她全是上下在好的意義上呈現出一幅自然體。

無論是姿態、性格、頭銜還是實力。正因為沒有任何裝飾，所以大家並沒能察覺到她的實力。

「那麼、如果在比試中有在意的地方的話，能說說看嗎？」

「明白了」

靠著愛麗絲大人，姑且算是把貝雅托麗克絲給樹立起來了的樣子。

就在這微妙的氛圍中，『武神祭』正式比賽的第二天開始了。

000

都艾姆進入特等室後，一個穿著灰色斗篷的人回過頭望了一下都艾姆。

雖然臉被斗篷遮住了，但從體格來看恐怕是個女人。她看了一下都艾姆，接著把目光轉向了都艾姆身旁的奧利雅納國王。

然後說了一句。

「好臭」

「喂、女人、很沒禮貌呢」

「抱歉」

都艾姆壓制住動搖的內心、瞪向灰袍的女人。

都艾姆為了使奧利雅納國王成為傀儡，使用了依賴性很強的藥草。雖然藥效無可挑剔，可卻有著中毒者會散發獨特味道的缺點。

但已經用香水蓋住這個味道了，應該不會被人察覺到。

「都艾姆閣下、這位是『武神』貝雅托麗克絲大人」

「什麼……」

『武神』貝雅托麗克絲，雖然聽說她來到王都了，但真的是本人嗎？

她看上去實在不像是被稱為『武神』的劍豪。

身著褪色的灰色斗篷且不知禮儀。僅一句抱歉便開始觀看起了比賽。

雖然看著不是很強……可若她的實力真如傳聞一樣的話，也有都艾姆無法看穿的可能性。考慮到愛麗絲公主都承認她的話，應該看作是本人吧。

聽說『武神』貝雅托麗克絲的素顏與英雄奧利維埃極其相似，雖然若能看到臉的話就明白了……

「那還真是、雖說不知但還真是失敬呢」

「我這邊才該抱歉」

最後以都艾姆與貝雅托麗克絲的道歉收場了，貝雅托麗克絲的失言被認為是對都艾姆說的。

都艾姆也想避免因為味道而引起的騷動。

但沒想到貝雅托麗克絲居然會在『武神祭』現身。

還偏偏是這一天……

都艾姆輕輕地咂了一下舌。

「米德嘉爾國王、貴安」

「嗚嗯」

轉換了心情，都艾姆向米德嘉爾國王打招呼。米德嘉爾國王正坐在特等室的巨大王座上。

做過定式的社交問候之後，奧利雅納國王也坐在了米德嘉爾國王的身旁。而在他的旁邊則坐著都艾姆，為奧利雅納國王打照應。

奧利雅納國王雖然能進行定式的應答，但更複雜的對話則很令人不安。這時就得由都艾姆來慢慢地誘導對話，為國王打照應了。

不過、到此為止還和計劃一樣。

都艾姆的首要目標是確保蘿茲。

最後見到她的時候，她已經發病了，對與教團來說她的血肯定會成為最好的資料。

為此、都艾姆撒了個餌。

如果蘿茲沒有在『武神祭』的會場現身的話，就以讓奧利雅納國王殺害米德嘉爾國王來威脅她。

當然這只是威脅，但都艾姆覺得即使真的這麼做了也無妨。

如果殺害米德嘉爾國王的話應該會引起戰爭導致奧利雅納王國的滅亡吧。不過將傀儡安置在米德嘉爾王國的下一任王座上的準備已經在進行當中了。如果順利進行的話便可獲得最大利益，雖然有失敗的風險，但更有嘗試的價值。

如果說有不確定要素的話，就是現場的愛麗絲了。可以看出她對呆滯的奧利雅納國王的持不信任的態度，計劃有可能會受到阻礙。

然而只要在愛麗絲比賽的途中殺死國王的話，這個不確定因素就很容易排除了，因此應該沒有任何問題才是。

可是，現在這裡有『武神』貝雅托麗克絲，想要除掉她非常困難，實力應該也在愛麗絲之上，如果她的行動妨礙到計劃的話，肯定會成為比愛麗絲更大的障礙。

而且也不清楚謎之男子吉米那的目的。他是裏世界的住人，肯定也是為了什麼目的而行動的，但查了一下背後的關係卻什麼也沒有查到。這傢伙是專業的，有必要做最大限度的警戒。

都艾姆大大地嘆了一口氣。

雖然仍按照計劃進行，但不確定要素太多了。實在難以說是能讓人放心的狀況。

但這也只要蘿茲在會場現身的話就沒有問題了，只要她現身的話就沒有必要去冒風險。

蘿茲肯定會出現的，她無法捨棄她的國家與父親，都艾姆是這麼認為的。

不定要素確實很多但沒有問題，所有的事應該都能順利進行。

都艾姆這樣對自己說著開始了觀戰。

就這樣時間流逝，比賽也毫無懸念的是克蕾雅·卡蓋諾的勝利。

「嚯……」

雖然不是特別矚目的選手，但她的實力也在意料之外。雖然她魔力很高，但卻沒有濫用的跡象。

即使現在也很強，但還能變得更強。

「克蕾雅小姐……本領漸長呢」

看到克蕾雅的勝利愛麗絲站了起來。

「我的比賽也快開始了，就先失陪了」

大家都鼓勵著愛麗絲，坐在愛麗絲身旁的黑髮少年也站了起來。

「我去趟廁所」

直接去不就好了，大家都這麼想。不對、只有貝雅托麗克絲，她的目光追著他的背影而去。

名為希德的平凡的少年，雖然在意他為什麼坐在愛麗絲公主的旁邊，不過除此以外也沒有什麼特別需要注意的地方。都艾姆在坐下的同時就忘掉了他的事情，將意識轉移到下一場比賽。

愛麗絲與吉米那的比賽對於都艾姆來說有著重要的意義。

不僅能夠看清裏世界住人吉米那的實力與目的，更重要的是、這也是愛麗絲離席的好機會。

二人離開特等室後，沒多久……愛麗絲和吉米那出現在了賽場中央。

000

愛麗絲一在比賽場地現身，便受到了熱烈的歡迎。

這超高的人氣正是、她才是這場大會主角的證明。

愛麗絲注視著對面的吉米那、讓自己冷靜下來。

吉米那·塞寧，他毫無疑問是個強敵。雖然即使這樣面對面也感受不到他的強大，但他就是有著某種深不可測的感覺。那與所見的印象毫不平衡的實力。讓人覺得不搭調的、迷惑真相的青年。

然而、愛麗絲並沒不認為自己贏不了。又或者說她不得不獲勝才對。

愛麗絲堅信、在『武神祭』上取得勝利便是自己的使命。

她沒有政治上的判斷力，就連她自己也承認這一點。她能做的就只有成為米德嘉爾王國強大的象徵而已。

讓人覺得只要有愛麗絲·米德嘉爾在王國就會平安無事，這才是她的使命。

即使被當作偶像來供奉也無所謂。只能以力量為武器的她，也很清楚自己那會因政治目的而被利用的立場。

沒錯、直到最近為止都是這樣的。

而作為被持續供奉的代價，當她試著獨自站起來時也開始逐漸暴露了出來。雖然她因擔憂國家的未來而成立『紅之騎士團』，但是無論是人員還是預算卻都籌集不起來，什麼都沒有改變。

雖然從那之後花費時間慢慢的將人聚集起來，但距離她所期望的形態還差的遠。

但是就算現在對政治方面出手，也只會落得***縱利用的下場。這樣的話就將政治方面交給別人，她只能在自己擅長的領域積蓄力量。

她很清楚來自國民的人氣會成為巨大的力量。能委任騎士團頭腦的人才也已經確保住了，之後只要她在『武神祭』上取得優勝，保持自己在國民當中的人氣的話，等著的她的肯定會是一個好結果。

這麼相信著，愛麗絲架起劍等待著比賽的開始。

雖然對吉米那很抱歉，但她從一開始就要全力以赴。不管他藏著什麼，都要讓他無暇使出就決出勝負。

「愛麗絲·米德嘉爾對吉米那·塞寧！！比賽開始！」

速攻。

愛麗絲在開始的同時向前衝去、然後停在了那裡。

「……誒？」

微小的疑問聲從她的口中漏出。

怎麼回事、總感覺吉米那的身影比想像中的遠。

是弄錯了間距嗎。

雖然這麼想，但實際並沒有弄錯。只是、感覺吉米那的身影仿彿在遙遠的地方。

並不清楚原因，也許是因為自己在緊張吧。

但不管怎麼說，她的腳已經停住了。

重新擺好了架勢。

愛麗絲轉換了一下心情，將劍架好、並輕輕地做了個假動作。

在確認吉米那的視線被引誘的一瞬間，她砍了過去。

然而。

「……！？」

又一次、她的動作停住了。

愛麗絲仿彿在躲避著什麼似得，後仰上身、向後跳開。

她看到了劍。

她看到了吉米那的劍斬下自己首級的畫面。

可是、吉米那的劍根本就沒有動。

當然、她的腦袋也還緊緊地連在她的脖子上。

「為什麼……？」

她不由得低語道。

她確實看到了吉米那的劍。

在她要砍下去的瞬間。看到了隱藏著壓倒性威力的吉米那的劍、梟首自己的場景。

完全被鎖定了。

敗北……不、她甚至做好死的覺悟。

可是、那卻仿佛像泡沫一般，吉米那甚至連劍都沒有架起，只是單純的站在那裡。

無法理解到底發生了什麼。

愛麗絲架起劍仿彿在試探吉米那似得、在他的周圍緩慢地繞著圈。

一圈、兩圈、三圈……

明明是和平時一樣的間距，可是不知道為何就是感覺吉米那的身影離自己很遠。

「……不來嗎？」

吉米那問道。

可是、卻無法邁出步子。

絕對不能踏出這一步，她的本能正這麼警告著她。

「哈啊啊啊啊啊啊！！」

愛麗絲就像要斬斷猶豫一樣咆哮著。

借著前後搖擺，向前衝去。這對於她來說最快的一步。

但是——被看穿了！！

吉米那的視線就這樣直直地捕捉到了她。

接著仿彿在暗示什麼一般，他的視線動了。

「——啊啊啊啊啊啊！」

那個瞬間，愛麗絲憑著本能止住了。

巨大的負荷襲向了身體，膝關節發出了令人討厭的聲音。

就算這樣也無妨，愛麗絲止住了腳步，宛若摔倒一般順勢向後跳去。

她確實看到了、吉米那的劍貫穿了自己的胸膛。

「……騙人的吧」

可是、她的胸口根本就沒有傷口。

吉米那連揮劍的痕跡都看不出。

「騙人……」

在她面前的吉米那甚至連劍都沒架起，直直地站在那裡。

「……怎麼了？」

他問道。

正體不明的東西東西讓愛麗絲的身體顫抖了一下。

如果不做點什麼的話。

焦躁與恐懼混合的情感衝擊著她。

同時、吉米那的視線又動了。

仿彿能夠預測未來一般，注視著未來他的劍微微的抖了一下。

那個瞬間、愛麗絲看到自己的手臂被斬斷的幻象。

「啊、啊……」

終於、她理解了一切。

吉米那不過只是做了一些假動作罷了。

他完全看穿了愛麗絲的動作，僅以視線和劍尖細微的抖動來警告著她。

如果不停下來的話我就砍下去了哦、就像這樣。

僅是如此、愛麗絲便幻視到他的劍。

將自己被砍的錯覺當成了現實。

過去老師教給自己的話在愛麗絲的腦中甦醒。「高手的『虛』會讓人錯以為是現實」，就如這句話所印證的那樣，小時候的愛麗絲也經常被老師的假動作戲弄。

可吉米那的這個，是遠在過去的老師之上的『真實』。

這樣的事，真的有可能嗎——？

愛麗絲並沒有覺得自己是世界最強的，她很理解人外有人這句話。可作為客觀的事實，即使放眼世界愛麗絲應該也是最上位的魔劍士，本應是這樣的。

如果能將這樣的她，僅以假動作戲弄的話。

吉米那的實力——毫無疑問是世界最強。

那是、任誰都不會輸的絕對的最強。

這樣的事，真的有可能嗎？

怎麼可能會有這種事。

愛麗絲對自己說道。

不要被騙了。

他連一次劍都沒揮過，不要僅憑臆測就擅自決定。

「……不要停下」

愛麗絲像是在對自己的本能說話似得嘟囔著。

她做好了絕對不停下來的覺悟，先前衝去。

響起了什麼斬斷空氣的聲音。

下個瞬間。

劇烈的衝擊襲擊了愛麗絲全身。

僅僅數秒失去了意識，回過神來她正在仰望著天空。

在比賽場地的中心，愛麗絲仰面倒下了。

到底發生了什麼。

愛麗絲沒能看到吉米那的劍。可吉米那的視線捕捉到了愛麗絲，同時劇烈的衝擊襲向了她。

沒有鬆開手中的劍簡直是奇跡。

愛麗絲撐起反應遲鈍的身體。

「愛麗絲·米德嘉爾……難道只有這種程度嗎？」

劍被插在自己的眼前。

吉米那那雙讀不出情感的眼睛俯視著愛麗絲。

遙遠的彼方……

啊……原來是這樣一回事。

愛麗絲終於理解了。

會覺得他的身影很遠，既不是錯覺也不是其他的什麼原因。

是他從一開始就在很遠很遠的地方俯視著她。愛麗絲不管怎樣伸出手來也觸碰不到的，遙遠的彼方……

劍從愛麗絲的手中落下，發出了冰冷的聲音。

在充滿寂靜的會場，那個聲音聽起來格外的響徹。

愛麗絲·米德嘉爾僅僅一擊就敗北了。

這個事實、仍誰都只能呆呆的發愣。

在寂靜之中。

咯噠、咯噠的腳步聲自愛麗絲的身後響起。

會場一點點的喧嚷了起來。

咯噠、咯噠的腳步聲徑直前進著，然後停在了那裡。

觀眾們任誰都注視著腳步聲的主人。

就連吉米那的臉上也浮現出一絲驚訝。

「我回來了，父親大人」

站在那裡的是美麗的奧利雅納王國的公主、蘿茲·奧利雅納。

絲毫沒有注意愛麗絲和吉米那，她那雙蜂蜜色的眼眸只是緊緊地盯著特等席。

000

僅一擊就打敗了那個愛麗絲・米德嘉爾。

在這個現實面前，都艾姆只是愕然地僵立著。

對於生活在世界陰暗面的都艾姆來說，確實知道超越愛麗絲・米德嘉爾的實力者。不過，縱使是艾姆所認識的最強魔劍士，又真的能一擊擊破愛麗絲嗎。

不。

若不是出其不意的突襲，或是純粹的碰巧，否則都是不可能的。

也就是說、這是不能夠存在的事。

否則僅憑一擊就擊破愛麗絲的吉米那，便將成為都艾姆認知中的最強魔劍士。

像這樣的小輩居然……！

被小輩從後趕超的瞬間，他的優越感也因而受挫。

都艾姆心中的驚愕，不知不覺間被熊熊燃燒的嫉妒之火所蓋過。

大腦不斷抗拒、並否定著吉米那。

愛麗絲被一擊打敗的要因，應該存在著偶然因素才是。即使那並非偶然，所謂戰鬥也存在著相性問題。說不定對於吉米那來說，愛麗絲恰好是個很好對付的對手也不一定。

除此之外、愛麗絲那匪夷所思的行徑也是疑點所在。像是忽然對著什麼產生警戒地止步，又無謂地在吉米那四周繞圈。是愛麗絲狀態欠佳嗎，還是說吉米那針對她的弱點耍了什麼小手段嗎。

可以否定吉米那實力的依據要多少有多少。

不過、即使如此。

都艾姆的本能，卻正要屈服於這名叫做吉米那男子的劍下。

他已經意識到、自己與吉米那所見的世界大有不同的事實。

無論是戰鬥的理論，還是根本思路都大相徑庭。即使自己在未來幾百年間不停鍛練，也絕對追趕不上這名青年。吉米那的劍就是如此這般地、經過了去蕪存菁的洗練。他那糅合各種武學優點的劍，簡直就像精雕細琢而成的、獨一無二的藝術作品一般。

在否定吉米那實力的同時，卻像個少年似得對吉米那的劍感到著迷。

那就像年幼時、仰慕師長那種情感一樣，吉米那的劍中有著讓武人為之傾倒的魔性。

嘎嘎地、都艾姆將牙咬地咯咯直響。

無法認同。

而且、這又不代表這名青年就是最強。

都艾姆認識很多實力者。可教團最高幹部的全力，至今也未能一窺。

所以最強的人，不是吉米那。

「貝雅托麗克絲大人對這場比賽怎麼看？」

為了聽到否定吉米那的話語，都艾姆這麼問道。

貝雅托麗克絲從罩袍中露出的蒼色雙眸，關注著吉米那。而那雙眸中飽含的是……感嘆之情。

「……想打一場看看」

「哈？」

正當都艾姆想要訊問這話的真意時，會場騷然了起來。

都艾姆一看比賽場地，出現在那裡的是……

「蘿茲・奧利雅納……」

都艾姆的表情像是嘲笑似得戚了起來。

來了嗎。

果然是愚蠢的女人。奧利雅納王國與國王都已經到了為時已晚的地步。傀儡的王已經失去了神智。多虧如此，也掌控了國家中樞。而沒理解到這點就坦然地現身，真是個天真過頭的公主。

都艾姆為免被人察覺到他在奸笑而掩住嘴巴，伴隨奧利雅納國王上前去。

「我心愛的蘿茲公主。你終於願意回來了」

從特別室通往比賽場地有一段台階。都艾姆伴隨著奧利雅納國王走下台階。

「蘿茲啊，歡迎你回來了。來、過來我這裡」

在都艾姆的指示下，奧利雅納國王放話。那是沒有心意的、空洞的話語。

都艾姆走下台階並打眼色向手下發出指示，準備隨時隨地拿下蘿茲。

蘿茲走上台階。

「父親大人，我是為謝罪而來的。至今為止的事、以及今後的事……我犯下了過錯，今後也仍會出錯吧。可是我、身為奧利雅納王國的公主，又身為你的女兒……我要走上我所相信的道路」

蘿茲的聲音顫抖著。那雙眸中也溢出了淚水。

但是、蘿茲的雙眸中仍有著決意。

都艾姆霎時間洞悉得到，後退一步。

總之先讓國王向前去。

以國王為盾的話，這女人就什麼也做不成。

只要成為傀儡的國王還在，都艾姆的計劃就是暢通無阻的。

「我寬恕你的罪」

奧利雅納國王說道。那是都艾姆還沒有指示的話語。

「感激不盡、父親大人」

那之後、僅僅只是一瞬間的事變。

蘿茲拔出腰間的劍，都艾姆作出反應，藏到國王身後。

都艾姆的手下們行動起來。

但蘿茲的動作實在太過迅速了。

都艾姆吃驚地瞪大雙眸。

「什！？」

將一切都放下的她，用細劍刺穿了奧利雅納國王的心臟。

「身為公主，又身為女兒……這是我最後的義務了」

王為了抱住蘿茲而伸出的手，途中無力地垂了下來。細劍確實地貫穿了王的心臟，同時也刺中了他身後的都艾姆的腹部。

「至今為止，感激不盡、父親大人」

然後她拔出了劍。

王的心臟處噴出鮮血，他倒了去。

她眼中的淚水，終於淌了下來。

「你、你這傢伙啊啊啊啊啊啊啊阿阿阿阿阿阿阿阿！！」

都艾姆絕叫著。

都艾姆的腹部也滲出了血。不過還不足以造成致命傷。

他的氣憤，在於喪失了傀儡。都艾姆的計劃——破碎了。

「還不快抓住她啊啊啊啊啊啊啊阿阿阿阿阿阿阿阿阿阿！！」

手下朝蘿茲一擁而上。

蘿茲沒有逃跑。

她只是以細劍的劍尖抵住自己的脖子、看著都艾姆露出了笑容。

難不成——

都艾姆的臉色蒼白了起來。

「住、住手、住手啊啊啊啊阿阿阿阿阿阿阿阿！！」

然後正當蘿茲想要動手用劍刺穿脖子、的那一瞬間。

「——這就是你的選擇嗎」

彷如藝術般優美的一閃，將蘿茲的劍、以及包圍著她的劍一舉掃除。

現身於那裡的是平凡的青年吉米那。

「你、你是……」

而他握在手中的、是宛如暗夜般深邃的漆黑之刃。

# HISTORY

## 2020-09-06

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 7 , c: 124, add: 0 )

## 2020-09-01

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 2 , c: 28, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-21

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 2 , c: 27, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 7 , c: 124, add: 3 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2020-08-11

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 26, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-08

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 25, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-06

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 24, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-08-05

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 23, add: 3 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 121, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 3 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )



